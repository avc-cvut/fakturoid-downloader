#!/bin/sh
set -eux

# shellcheck disable=SC2038
find . -name '*.sh' -type f | xargs shellcheck
