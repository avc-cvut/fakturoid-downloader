#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -x
# project root
cd "$(dirname "$DIR")"

cargo +nightly fmt --all -- "$@"