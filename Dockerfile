FROM rust:1.48.0 as build
WORKDIR /usr/src

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update \
 && apt-get install -y musl-tools \
 && rm -rf /var/lib/apt/lists/* \
 && rustup target add x86_64-unknown-linux-musl

# trick to cache dependecies
# see https://github.com/rust-lang/cargo/issues/2644
WORKDIR /usr/src
RUN USER=root cargo new oldarchive-gdrive-http-gateway
WORKDIR /usr/src/oldarchive-gdrive-http-gateway

COPY Cargo.toml Cargo.lock ./
RUN cargo install --target x86_64-unknown-linux-musl --locked --path .
RUN rm -rf src target/release/.fingerprint/fakturoid-downloader*

COPY src ./src

RUN cargo install --target x86_64-unknown-linux-musl --locked --path .

# Test that everything is staticly linked. Copy to /dev/stderr for observability
RUN ldd "/usr/local/cargo/bin/fakturoid-downloader" | tee /dev/stderr | grep -q "statically linked"

ENTRYPOINT ["/usr/local/cargo/bin/fakturoid-downloader"]

# -------------------------------------------------

FROM alpine:3.12 as alpine-ca-certificates

RUN apk add --no-cache ca-certificates

# -------------------------------------------------

FROM scratch as release
WORKDIR /

COPY --from=alpine-ca-certificates /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /usr/local/cargo/bin/fakturoid-downloader /fakturoid-downloader

ENTRYPOINT ["/fakturoid-downloader"]
