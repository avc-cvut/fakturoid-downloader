use std::io;

use reqwest::header::HeaderMap;
use serde::{Deserialize, Serialize};
use yup_oauth2::{
    self as oauth2,
    authenticator::DefaultAuthenticator,
    AccessToken,
};

static FOLDER_MIME_TYPE: &str = "application/vnd.google-apps.folder";

fn get_authenticator(sa_key_data: &str) -> DefaultAuthenticator {
    let mut runtime = tokio::runtime::Runtime::new().unwrap();

    let sa_key: oauth2::ServiceAccountKey = serde_json::from_str(&sa_key_data)
        .map_err(|e| {
            io::Error::new(
                io::ErrorKind::InvalidData,
                format!("Bad service account key: {}", e),
            )
        })
        .unwrap();

    let authenticator = runtime
        .block_on(oauth2::ServiceAccountAuthenticator::builder(sa_key).build());

    authenticator.unwrap()
}

fn get_token(
    authenticator: &DefaultAuthenticator,
) -> Result<AccessToken, AppTokenError> {
    let mut runtime = tokio::runtime::Runtime::new().unwrap();

    runtime
        .block_on(
            authenticator.token(&["https://www.googleapis.com/auth/drive"]),
        )
        .map_err(AppTokenError)
}

pub fn get_client(sa_key_data: &str) -> GDriveClient {
    GDriveClient::new(get_authenticator(sa_key_data))
}

pub struct GDriveClient {
    authenticator: DefaultAuthenticator,
    client: reqwest::blocking::Client,
}

impl GDriveClient {
    pub fn new(authenticator: DefaultAuthenticator) -> GDriveClient {
        let client = reqwest::blocking::ClientBuilder::new()
            .gzip(true)
            .build()
            .unwrap();

        GDriveClient {
            authenticator,
            client,
        }
    }

    fn get_token(&self) -> Result<AccessToken, AppTokenError> {
        let mut runtime = tokio::runtime::Runtime::new().unwrap();

        runtime
            .block_on(
                self.authenticator
                    .token(&["https://www.googleapis.com/auth/drive"]),
            )
            .map_err(AppTokenError)
    }

    pub fn list_files(&self, query: &str) -> FilesListResponse {
        let token = self.get_token().unwrap();

        let req = self
            .client
            .get("https://www.googleapis.com/drive/v3/files")
            .header(
                reqwest::header::AUTHORIZATION,
                format!("Bearer {}", token.as_str()),
            )
            .query(&[("q", query)])
            .query(&[("supportsAllDrives", "true")])
            .query(&[("includeItemsFromAllDrives", "true")])
            .query(&[(
                "fields",
                "files(kind,id,name,mimeType,parents,modifiedTime) ",
            )]);

        let response = req.send();

        let response = response.unwrap();

        match response.status().as_u16() {
            200 => {
                let headers = response.headers().to_owned();
                let body = response.json::<FileListResponseBody>().unwrap();

                FilesListResponse { headers, body }
            },
            _ => {
                println!("{:#?}", response);
                println!("{:#?}", response.text().unwrap());

                panic!("Request Failed")
            },
        }
    }

    pub fn create_folder(
        &self,
        folder_name: &str,
        parent_id: &str,
    ) -> BaseFile {
        let token = self.get_token().unwrap();

        #[derive(Debug, Serialize)]
        #[serde(rename_all = "camelCase")]
        struct CreateFile {
            name: String,
            parents: Vec<String>,
            mime_type: String,
        }

        self.client
            .post("https://www.googleapis.com/drive/v3/files")
            .header(
                reqwest::header::AUTHORIZATION,
                format!("Bearer {}", token.as_str()),
            )
            .json(&CreateFile {
                name: folder_name.into(),
                parents: vec![parent_id.into()],
                mime_type: FOLDER_MIME_TYPE.to_owned(),
            })
            .query(&[("supportsAllDrives", "true")])
            .send()
            .unwrap()
            .json()
            .unwrap()
    }

    pub fn get_folder(
        &self,
        folder_name: &str,
        parent_id: &str,
    ) -> Option<File> {
        self.list_files(&format!(
            "name = '{}' and '{parent_id}' in parents and trashed = false and mimeType = '{mime_type}'",
            name = folder_name,
            parent_id = parent_id,
            mime_type = FOLDER_MIME_TYPE
        ))
        .body
        .files
        .first()
        .map(|i| (*i).clone())
    }

    pub fn get_or_create_folder(
        &self,
        folder_name: &str,
        parent_id: &str,
    ) -> BaseFile {
        let tmp = self.get_folder(&folder_name, &parent_id);
        tmp.map(BaseFile::from)
            .unwrap_or_else(|| self.create_folder(folder_name, parent_id))
    }

    pub fn get_file(&self, name: &str, parent_id: &str) -> Option<File> {
        self.list_files(&format!(
            "name = '{}' and '{parent_id}' in parents and trashed = false and mimeType != '{mime_type}'",
            name = name,
            parent_id = parent_id,
            mime_type = FOLDER_MIME_TYPE
        ))
        .body
        .files
        .first()
        .map(|i| (*i).clone())
    }

    pub fn create_file(&self, metadata: &CreateFileMetadata, data: Vec<u8>) {
        let token = self.get_token().unwrap();

        let metadata_response = self
            .client
            .post("https://www.googleapis.com/upload/drive/v3/files")
            .query(&[("supportsAllDrives", "true")])
            .query(&[("uploadType", "resumable")])
            .header(
                reqwest::header::AUTHORIZATION,
                format!("Bearer {}", token.as_str()),
            )
            .json(metadata)
            .send()
            .unwrap();

        let session_uri = metadata_response
            .headers()
            .get("Location")
            .unwrap()
            .to_str()
            .unwrap();

        self.client
            .put(session_uri)
            .header(
                reqwest::header::AUTHORIZATION,
                format!("Bearer {}", token.as_str()),
            )
            .body(data)
            .send()
            .unwrap();
    }

    // TODO: Update a create je skoro stejnej, stalo by za to sjednotit
    pub fn update_file_content(
        &self,
        id: &str,
        metadata: &UpdateFileContentMetadata,
        data: Vec<u8>,
    ) {
        let token = self.get_token().unwrap();

        let metadata_response = self
            .client
            .patch(&format!(
                "https://www.googleapis.com/upload/drive/v3/files/{}",
                id
            ))
            .query(&[("supportsAllDrives", "true")])
            .query(&[("uploadType", "resumable")])
            .header(
                reqwest::header::AUTHORIZATION,
                format!("Bearer {}", token.as_str()),
            )
            .json(metadata)
            .send()
            .unwrap();

        let session_uri = metadata_response
            .headers()
            .get("Location")
            .unwrap()
            .to_str()
            .unwrap();

        self.client
            .put(session_uri)
            .header(
                reqwest::header::AUTHORIZATION,
                format!("Bearer {}", token.as_str()),
            )
            .body(data)
            .send()
            .unwrap();
    }
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CreateFileMetadata {
    pub name: String,
    pub parents: Vec<String>,
    pub created_time: chrono::DateTime<chrono::Utc>,
    pub modified_time: chrono::DateTime<chrono::Utc>,
    pub mime_type: String,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateFileContentMetadata {
    pub modified_time: chrono::DateTime<chrono::Utc>,
    pub mime_type: String,
}

#[derive(Debug)]
pub struct AppTokenError(oauth2::Error);

#[derive(Debug, Clone)]
pub struct FilesListResponse {
    pub headers: HeaderMap,
    pub body: FileListResponseBody,
}

#[derive(Debug, Deserialize, Clone)]
pub struct FileListResponseBody {
    pub files: Vec<File>,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct BaseFile {
    pub kind: String,
    pub id: String,
    pub name: String,

    pub mime_type: String,
}

impl From<File> for BaseFile {
    fn from(file: File) -> Self {
        BaseFile {
            kind: file.kind,
            id: file.id,
            name: file.name,
            mime_type: file.mime_type,
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct File {
    pub kind: String,
    pub id: String,
    pub name: String,

    pub mime_type: String,
    pub parents: Vec<String>,
    pub modified_time: chrono::DateTime<chrono::Utc>,
}

impl File {
    pub fn is_folder(&self) -> bool {
        self.mime_type == FOLDER_MIME_TYPE
    }
}
