use chrono::Datelike;
use fakturoid_downloader::{
    fakturoid::FakturoidClient,
    gdrive::{
        get_client,
        CreateFileMetadata,
        GDriveClient,
        UpdateFileContentMetadata,
    },
};
use std::collections::hash_map::{Entry, HashMap};

fn main() {
    let gdrive = get_google_drive_from_env();
    let invoices_folder = std::env::var("GDRIVE_TAGET_FOLDER")
        .expect("Env GDRIVE_TAGET_FOLDER not defined");

    let fakturoid = get_fakturoid_from_env();
    let invoices_source = fakturoid.list_invoices(1);

    let mut folders_cache: HashMap<String, String> = HashMap::new();

    println!("Starting FakturoidSync");

    for invoice in invoices_source.invoices {
        print!("Processing invoice {}...", invoice.number);

        let invoice_year = invoice.issued_on.year().to_string();

        let year_folder_id = match folders_cache.entry(invoice_year.clone()) {
            Entry::Vacant(entry) => entry
                .insert(
                    gdrive
                        .get_or_create_folder(&invoice_year, &invoices_folder)
                        .id
                        .clone(),
                )
                .clone(),
            Entry::Occupied(entry) => entry.get().clone(),
        };

        let file_name = format!("{}.pdf", &invoice.number);

        match gdrive.get_file(&file_name, &year_folder_id) {
            Some(drive_file) => {
                if drive_file.modified_time < invoice.updated_at {
                    gdrive.update_file_content(
                        &drive_file.id,
                        &UpdateFileContentMetadata {
                            mime_type: "application/pdf".to_owned(),
                            modified_time: invoice.updated_at,
                        },
                        fakturoid.download_invoice(&invoice.id).to_vec(),
                    );
                    println!("Updated");
                } else {
                    println!("Nothing to do");
                }
            },
            None => {
                gdrive.create_file(
                    &CreateFileMetadata {
                        name: file_name,
                        mime_type: "application/pdf".to_owned(),
                        created_time: invoice.created_at,
                        modified_time: invoice.updated_at,
                        parents: vec![year_folder_id.clone()],
                    },
                    fakturoid.download_invoice(&invoice.id).to_vec(),
                );
                println!("Created");
            },
        }
    }
}

pub fn get_google_drive_from_env() -> GDriveClient {
    let mut sa_file_location =
        std::fs::File::open(get_env_force("GDRIVE_SA_FILE"))
            .expect("Could not open sa file");

    let mut sa_key_data = String::new();
    std::io::Read::read_to_string(&mut sa_file_location, &mut sa_key_data)
        .expect("Could not read from sa file");

    get_client(&sa_key_data)
}

pub fn get_fakturoid_from_env() -> FakturoidClient {
    FakturoidClient::new(
        get_env_force("FAKTUROID_EMAIL"),
        get_env_force("FAKTUROID_API_KEY"),
        get_env_force("FAKTUROID_SLUG"),
    )
}

pub fn get_env_force(key: &str) -> String {
    std::env::var(key).unwrap_or_else(|_| panic!("Env var {} not set", key))
}
