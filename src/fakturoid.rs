use bytes::Bytes;
use chrono::NaiveDate;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct InvoicesCollection {
    pub invoices: Vec<InvoicesCollectionInvoice>,
}

#[derive(Deserialize, Debug)]
pub struct InvoicesCollectionInvoice {
    pub custom_id: Option<String>,
    pub id: i64,
    // pub proforma: Option<bool>,
    // pub partial_proforma: Option<bool>,
    pub number: String,
    // pub variable_symbol: Option<String>,
    // pub your_name: String,
    // pub your_street: String,
    // pub your_street2: String,
    // pub your_city: String,
    // pub your_zip: String,
    // pub your_country: String,
    // pub your_registration_no: String,
    // pub your_vat_no: String,
    // pub your_local_vat_no: String,
    // pub client_name: String,
    // pub client_street: String,
    // pub client_street2: String,
    // pub client_city: String,
    // pub client_zip: String,
    // pub client_country: String,
    // pub client_registration_no: String,
    // pub client_vat_no: String,
    // pub client_local_vat_no: String,
    // pub subject_id: i64,
    // pub subject_custom_id: Option<i64>,
    // pub generator_id: Option<i64>,
    // pub related_id: Option<i64>,
    // pub correction: Option<bool>,
    // pub correction_id: Option<i64>,
    // pub token: String,
    // pub status: String,
    // pub order_number: Option<String>,
    pub issued_on: NaiveDate,
    // pub taxable_fulfillment_due: Option<chrono::DateTime<chrono::Utc>>,
    // pub due: Option<i64>,
    // pub due_on: chrono::DateTime<chrono::Utc>,
    // pub sent_at: chrono::DateTime<chrono::Utc>,
    // pub paid_at: chrono::DateTime<chrono::Utc>,
    // pub reminder_sent_at: chrono::DateTime<chrono::Utc>,
    // pub accepted_at: chrono::DateTime<chrono::Utc>,
    // pub cancelled_at: chrono::DateTime<chrono::Utc>,
    // pub note: Option<String>,
    // pub footer_note: Option<String>,
    // pub private_note: Option<String>,
    // pub tags: Option<String>,
    // pub bank_account_id: i64,
    // pub bank_account: Option<String>,
    // pub iban: Option<String>,
    // pub swift_bic: Option<String>,
    // pub payment_method: Option<String>,
    // pub currency: Option<String>,
    // pub exchange_rate: Option<String>,
    // pub paypal: Option<bool>,
    // pub gopay: Option<bool>,
    // pub language: Option<String>,
    // pub transferred_tax_liability: Option<bool>,
    // pub supply_code: Option<i64>,
    // pub eu_electronic_service: Option<bool>,
    // pub vat_price_mode: Option<VatPriceMode>,
    // pub round_total: Option<bool>,
    // pub subtotal: f64,
    // pub native_subtotal: f64,
    // pub total: f64,
    // pub native_total: f64,
    // pub remaining_amount: f64,
    // pub remaining_native_amount: f64,
    // pub paid_amount: f64,
    // pub eet: Option<bool>,
    // pub eet_cash_register: Option<String>,
    // pub eet_store: Option<i64>,
    // pub eet_records: Option<array>,
    // pub attachment: Option<object>
    // pub public_html_url: String,
    // pub url: String,
    pub pdf_url: String,
    // pub subject_url: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "snake_case")]
pub enum VatPriceMode {
    WithoutVat,
    FromTotalWithVat,
}

pub struct FakturoidClient {
    email: String,
    api_key: String,
    slug: String,
    client: reqwest::blocking::Client,
}

impl FakturoidClient {
    const BASE_URL: &'static str = "https://app.fakturoid.cz/api/v2";
    const USER_AGENT: &'static str = "FakturoidDownloader (it@avc-cvut.cz)";

    pub fn new(
        email: String,
        api_key: String,
        slug: String,
    ) -> FakturoidClient {
        let client = reqwest::blocking::ClientBuilder::new()
            .gzip(true)
            .build()
            .unwrap();

        FakturoidClient {
            email,
            api_key,
            slug,
            client,
        }
    }

    pub fn list_invoices(&self, page: i32) -> InvoicesCollection {
        let url: String = format!(
            "{base_url}/accounts/{slug}/invoices.json",
            base_url = FakturoidClient::BASE_URL,
            slug = self.slug
        );

        let req = self
            .client
            .get(&url)
            .header("User-Agent", FakturoidClient::USER_AGENT)
            .basic_auth(&self.email, Some(&self.api_key))
            .query(&[("page", page)]);

        let res = req.send().unwrap();
        let result = res.json::<Vec<InvoicesCollectionInvoice>>().unwrap();

        InvoicesCollection { invoices: result }
    }

    pub fn download_invoice(&self, id: &i64) -> Bytes {
        let url: String = format!(
            "{base_url}/accounts/{slug}/invoices/{id}/download.pdf",
            base_url = FakturoidClient::BASE_URL,
            slug = self.slug,
            id = id,
        );

        let req = self
            .client
            .get(&url)
            .header("User-Agent", FakturoidClient::USER_AGENT)
            .basic_auth(&self.email, Some(&self.api_key));

        req.send().unwrap().bytes().unwrap()
    }
}
